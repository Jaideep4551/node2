import * as mongoose from 'mongoose';

export const UserModel = new mongoose.Schema({
    id: String,
    first_name: String,
    last_name: String,
    email: String,
    phone: String,
    address: String,
    created_at: { type: Date, default: Date.now }
})