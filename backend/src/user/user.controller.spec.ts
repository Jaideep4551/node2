import { Test, TestingModule } from '@nestjs/testing';
import { response } from 'express';
import { CreateUserDTO } from './dto/create-user.dto';
import { UserController } from './user.controller';
import { UserService } from './user.service';

describe('User Controller', () => {
  let controller: UserController;


  const mockUserService ={

    create: jest.fn(dto => {
      return {
        id: Date.now(),
        ...dto
      };
    }),
    update: jest.fn().mockImplementation((UserId)=>{
     UserId
    }),
  }

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [UserController],
      providers: [UserService],
    })
    .overrideProvider(UserService)
    .useValue(mockUserService)
    .compile();

    controller = module.get<UserController>(UserController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  it('should create a user',() =>{
    const dto = {response,first_name: 'jai',last_name:'deep',email:'abc@gmail.com',phone:'123',gender:'male',address:'ajb'}
    expect(controller.addUser(response,dto)).toEqual({
      id: Date.now(),
      first_name:'jai',
      last_name:'deep',
      email:'abc@gmail.com',
      phone:'123',
      gender:'male',
      address:'ajb'

    });

    expect(mockUserService.create).toHaveBeenCalledWith(dto);
  });

  it('should update a user', () =>{
    const dto = {first_name: 'jai',last_name:'deep',email:'abc@gmail.com',phone:'123',gender:'male',address:'ajb'};
    expect(controller.updateUser(response,'1',dto)).toEqual({
      id:1,
      ...dto
    });
    expect(mockUserService.update).toHaveBeenCalled();
  })
});
