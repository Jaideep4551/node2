import { Controller, Post, Res, Body, HttpStatus, Get, Param, NotFoundException, Put, Query, Delete } from '@nestjs/common';
import { UserService } from './user.service';
import { CreateUserDTO } from './dto/create-user.dto';


@Controller('record')
export class UserController {
    constructor(private userService: UserService) { }

    @Post('/create')
    async addUser(@Res() response, @Body() createUserDTO: CreateUserDTO) {
        const user = await this.userService.addUser(createUserDTO);
        return response.status(HttpStatus.OK).json({
            message: "User has been created successfully",
            user
        })
    }

    @Get('records')
    async getAllUsers(@Res() response) {
        const users = await this.userService.getAllUser();
        return response.status(HttpStatus.OK).json(users);
    }

    @Get('record/:UserId')
    async getUser(@Res() response, @Param('UserId') UserId) {
        const user = await this.userService.getUser(UserId);

        if (!user) {
            throw new NotFoundException('User does not exists!');
        }

        return response.status(HttpStatus.OK).json(user);
    }

    @Put('/update')
    async updateUser(@Res() response, @Query('UserId') UserId, @Body() createUserDTO) {
        const user = await this.userService.updateUser(UserId, createUserDTO);
        if (!user) {
            throw new NotFoundException('User does not exists!');
        }

        return response.status(HttpStatus.OK).json({
            message: 'User has been updated successfully!',
            user
        });
    }

    @Delete('/delete')
    async deleteUser(@Res() response, @Query('UserId') UserId) {
        const user = await this.userService.deleteUser(UserId);

        if (!user) {
            throw new NotFoundException('User does not exists!');
        }

        return response.status(HttpStatus.OK).json({
            message: 'User has been successfully deleted',
            user
        })
    }
}
