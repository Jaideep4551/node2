import { Module } from '@nestjs/common';
import { UserModel } from './schemas/user.schema';
import { UserService } from './user.service';
import { UserController } from './user.controller';
import { MongooseModule } from '@nestjs/mongoose';

@Module({
  imports: [
    MongooseModule.forFeature([{ name: 'User', schema: UserModel}])
  ],
  providers: [UserService],
  controllers: [UserController]
})
export class UserModule {}