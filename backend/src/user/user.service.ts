import { Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { User } from './interfaces/user.interface';
import { CreateUserDTO } from './dto/create-user.dto';

@Injectable()
export class UserService {
    constructor(@InjectModel('User') private readonly userModel: Model<User>) {}

    async addUser(createUserDTO: CreateUserDTO): Promise<User> {
        const user = await new this.userModel(createUserDTO);
        return user.save();
    }
    async getAllUser(): Promise<User[]> {
        const users = await this.userModel.find().exec();
        return users;
    }

    async getUser(userId): Promise<User> {
        const user = await this.userModel.findById(userId);
        return user;
    }

    async updateUser(userId, createUserDTO: CreateUserDTO): Promise<User> {
        const updatedUser = await this.userModel.findByIdAndUpdate(userId, createUserDTO, { new: true });
        return updatedUser;
    }

    async deleteUser(userId): Promise<any> {
        const deletedCustomer = await this.userModel.findByIdAndRemove(userId);
        return deletedCustomer;
    }
}