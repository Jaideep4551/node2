import React, { useEffect, useState } from 'react';
import logo from './logo.svg';
import './App.css';
import { UserAPI } from './api/user.api';
import { UserDTO } from './api/dto/user.dto';
import {Grid,AppBar,Toolbar,IconButton,Typography, Button,} from '@material-ui/core';
import User from './components/User';
import CreateUserModal from './components/CreateUserModal';
import EditUserModal from './components/EditUserModal';


function App() {
  const [users, setUsers] = useState<UserDTO[]>([])
  const [user, setUser] = useState<UserDTO[]>([])
  const [createUserModalOpen, setCreateUserModalOpen] = useState(false);
  const [updateUserModalOpen, setUpdateUserModalOpen] = useState(false);
  const [userBeingEdited, setUserBeingEdited] = useState<undefined | UserDTO>(undefined);

const addUser = (user: UserDTO) =>{
  setUsers([...users,user])
};

const deleteUser = (userId:string) =>{
  setUsers(users.filter(x=>x.userId !== userId));
}

const updateUser = (users:UserDTO) =>{
  setUsers(
    user.map((x) => {
    if(x.userId === users.userId)
    return users;
    return x;
  })
  );
};

useEffect(() =>{
  async function fetchAll(){
    const resp = await UserAPI.getAll();

    setUsers(resp);
  }
  fetchAll();

}, [])

const onUserEditBtnClicked = (users: UserDTO) =>{

}

  return (
    <div className="App">
      <CreateUserModal 
      open={createUserModalOpen} 
      handleClose={()=> setCreateUserModalOpen(false)}
      onUserCreated={addUser}
      />
      <EditUserModal
      data={userBeingEdited}
      open={updateUserModalOpen}
      handleClose={()=> setUpdateUserModalOpen(false)}
      onUserUpdated ={updateUser}
      />
      <AppBar position="static">
        <Toolbar>
          <Typography variant="h6" style={{flexGrow:1}}>
            User Records
          </Typography>
          <Button variant="contained" 
          color="primary" 
          onClick={()=> setCreateUserModalOpen(true)}>
          Create User</Button>
        </Toolbar>
      </AppBar>

      <Grid container spacing={1} style={{padding:50}}>
      {users.map((users) => {
          return (
          <Grid item xs ={3}>
          
          <User 
          data={users} 
          onUserDelete={deleteUser}  
          onUserUpdated = {(users: UserDTO) =>{
            setUserBeingEdited(users);
            setUpdateUserModalOpen(true);
          }}
            />
          
           </Grid>
          );
      })}
      </Grid>
    </div>
  );
     
}

export default App;
