export interface UpdateUserDTO{
    userId: string;
    first_name: string;
    last_name: string;
    email: string;
    phone: string;
    gender: string;
    address: string;
}