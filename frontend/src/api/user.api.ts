import { StringLiteralLike } from "typescript";
import { CreateUserDTO } from "./dto/create-user.dto";
import { UpdateUserDTO } from "./dto/update-user.dto";
import { UserDTO } from "./dto/user.dto";


export class UserAPI{
   public static async getAll(): Promise<UserDTO[]>{
        const resp = await fetch("http://localhost:5000/record/records",{
            method: "GET"
        })
        const data = await resp.json();

        return data;
    }
    public static async createOne(createRequest: CreateUserDTO){
        const resp = await fetch("http://localhost:5000/record/create",{
            method: "POST",
            headers:{
                'Content-type' : 'application/json'
            },
            body: JSON.stringify(createRequest)
        })

        const data = await resp.json();

        return data;
    }

    public static async deleteOne(userId: string){
     await fetch(`http://localhost:5000/record/delete/${userId}`,{
            method: "DELETE",
        })
    }
    public static async updateOne(userId:string,updateRequest:UpdateUserDTO){
        const resp = await fetch(`http://localhost:5000/record/update/${userId}`,{
            method: "PUT",
            headers:{
                'Content-type' : 'application/json'
            },
            body: JSON.stringify(updateRequest)
        })

        const data = await resp.json();

        return data;

    }
}