import { Button, createStyles, makeStyles, MenuItem, Modal, Select, TextField, Theme } from '@material-ui/core';
import { userInfo } from 'node:os';
import React, { useState } from 'react';
import { UserDTO } from '../api/dto/user.dto';
import { UserAPI } from '../api/user.api';

interface Props{
    open: boolean;
    handleClose: () => void;
    onUserCreated: (user:UserDTO) => void;

}

function getModalStyle() {
    const top = 50;
    const left = 50;
  
    return {
      top: `${top}%`,
      left: `${left}%`,
      transform: `translate(-${top}%, -${left}%)`,
    };
  }
  
const CreateUserModal = (props:Props) =>{
  const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    paper: {
      position: 'absolute',
      width: 400,
      backgroundColor: theme.palette.background.paper,
      border: '2px',
      boxShadow: theme.shadows[5],
      padding: theme.spacing(2, 4, 3),
    },
    textField:{
      width: "100%",
      padding:5
    },
    createBtn:{
      width:'100%',
      marginTop:10
    }
  }),
);


const [modalStyle] = React.useState(getModalStyle);
const [open, setOpen] = React.useState(false);
const classes = useStyles();

const [userId,setUserId] = useState('');
const [first_name,setFirstName] = useState('');
const [last_name,setLastName] = useState('');
const [email,setEmail] = useState('');
const [phone,setPhone] = useState('');
const [gender,setGender] = useState('');
const [address,setAddress] = useState('');


const createUser = async () =>{
  const resp = await UserAPI.createOne({
    userId,
    first_name,
    last_name,
    email,
    phone,
    gender,
    address,
  });
  props.onUserCreated(resp);
  console.log("New User",resp);
}

const body = (
  <div style={modalStyle} className={classes.paper}>
    <h2 id="simple-modal-title">Create New User</h2>
    <TextField
     placeholder = "User Id" 
     variant="filled" 
     className = {classes.textField}
     onChange={(e) => setUserId(e.target.value)}
    /> 
    <TextField
     placeholder = "FirstName" 
     variant="filled" 
     className = {classes.textField}
     onChange={(e) => setFirstName(e.target.value)}
    /> 
     <TextField
     placeholder = "LastName" 
     variant="filled" 
     className = {classes.textField}
     onChange={(e) => setLastName(e.target.value)}
    />
    <TextField
     placeholder = "Email" 
     variant="filled" 
     className = {classes.textField}
     onChange={(e) => setEmail(e.target.value)}
    />
    <TextField
     placeholder = "Phone" 
     variant="filled" 
     className = {classes.textField}
     onChange={(e) => setPhone(e.target.value)}
    />
     <TextField
     placeholder = "Gender" 
     variant="filled" 
     className = {classes.textField}
     onChange={(e) => setGender(e.target.value)}
    /> 
    <TextField
     placeholder = "Address" 
     variant="filled" 
     className = {classes.textField}
     onChange={(e) => setAddress(e.target.value)}
    />
    <Button 
    color = "primary" 
    variant="contained" 
    className={classes.createBtn} 
    onClick={createUser}
    >
      Create New User
      </Button>
  </div>
);

    return (
        <div>
 <Modal
        open={props.open}
        onClose={props.handleClose}
        aria-labelledby="simple-modal-title"
        aria-describedby="simple-modal-description"
      >
        {body}
      </Modal>
        </div>
    )
}

export default CreateUserModal;