import { Button, createStyles, makeStyles, Modal, TextField, Theme } from '@material-ui/core';
import React, { useEffect, useState } from 'react';
import { UserDTO } from '../api/dto/user.dto';
import { UserAPI } from '../api/user.api';

interface Props{
    open: boolean;
    handleClose: () => void;
    onUserUpdated: (users:UserDTO) => void;
    data:UserDTO | undefined;
}

function getModalStyle() {
    const top = 50;
    const left = 50;
  
    return {
      top: `${top}%`,
      left: `${left}%`,
      transform: `translate(-${top}%, -${left}%)`,
    };
  }
  
const EditUserModal = (props:Props) =>{
  const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    paper: {
      position: 'absolute',
      width: 400,
      backgroundColor: theme.palette.background.paper,
      border: '2px',
      boxShadow: theme.shadows[5],
      padding: theme.spacing(2, 4, 3),
    },
    textField:{
      width: "100%",
      padding:5
    },
    createBtn:{
      width:'100%',
      marginTop:10
    }
  }),
);


const [modalStyle] = React.useState(getModalStyle);
const [open, setOpen] = React.useState(false);
const classes = useStyles();

const [userId,setUserId] = useState('');
const [first_name,setFirstName] = useState('');
const [last_name,setLastName] = useState('');
const [email,setEmail] = useState('');
const [phone,setPhone] = useState('');
const [gender,setGender] = useState('');
const [address,setAddress] = useState('');

useEffect(() => {
    if(props.data) {
        setUserId(props.data.userId);
        setFirstName(props.data.first_name);
        setLastName(props.data.last_name);
        setEmail(props.data.email);
        setPhone(props.data.phone);
        setGender(props.data.gender);
        setAddress(props.data.address);
    }
    
},[props.data])


const UpdateUser = async () =>{
    if(props.data) {
        const resp = await UserAPI.updateOne(props.data.userId,{
            userId,
            first_name,
            last_name,
            email,
            phone,
            gender,
            address,
          });
  
  props.onUserUpdated(resp);
  console.log("New User", resp);
}
};

const body = (
  <div style={modalStyle} className={classes.paper}>
    <h2 id="simple-modal-title">Update User</h2>
    <TextField
     placeholder = "User Id" 
     variant="filled" 
     className = {classes.textField}
     onChange={(e) => setUserId(e.target.value)}
     value={userId}
    /> 
    <TextField
     placeholder = "FirstName" 
     variant="filled" 
     className = {classes.textField}
     onChange={(e) => setFirstName(e.target.value)}
     value={first_name}
    /> 
     <TextField
     placeholder = "LastName" 
     variant="filled" 
     className = {classes.textField}
     onChange={(e) => setLastName(e.target.value)}
     value={last_name}
    />
    <TextField
     placeholder = "Email" 
     variant="filled" 
     className = {classes.textField}
     onChange={(e) => setEmail(e.target.value)}
     value={email}
    />
    <TextField
     placeholder = "Phone" 
     variant="filled" 
     className = {classes.textField}
     onChange={(e) => setPhone(e.target.value)}
     value={phone}
    />
    <TextField
     placeholder = "Gender" 
     variant="filled" 
     className = {classes.textField}
     onChange={(e) => setGender(e.target.value)}
     value={gender}
    />
    <TextField
     placeholder = "Address" 
     variant="filled" 
     className = {classes.textField}
     onChange={(e) => setAddress(e.target.value)}
     value={address}
    />
    <Button 
    color = "primary" 
    variant="contained" 
    className={classes.createBtn} 
    onClick={UpdateUser}
    >
      Update User
      </Button>
  </div>
);

    return (
        <div>
 <Modal
        open={props.open}
        onClose={props.handleClose}
        aria-labelledby="simple-modal-title"
        aria-describedby="simple-modal-description"
      >
        {body}
      </Modal>
        </div>
    )
}

export default EditUserModal;