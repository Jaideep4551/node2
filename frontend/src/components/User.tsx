import React from 'react';
import {UserDTO} from '../api/dto/user.dto';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import { UserAPI } from '../api/user.api';


interface Props{
    data: UserDTO;
    onUserDelete: (userId:string) => void;
    onUserUpdated: (users:UserDTO) => void;
}

const User = ({data,onUserDelete,onUserUpdated}:Props) =>{

  const deleteUser = async () =>{
    await UserAPI.deleteOne(data.userId);
    onUserDelete(data.userId);
  }
    return (
    <Card variant="outlined">
    <CardContent>
    <Typography 
      color="textSecondary" 
      gutterBottom
      >
        {data.userId}
      </Typography>
      <Typography 
      color="textSecondary" 
      gutterBottom
      >
        {data.first_name}
      </Typography>
      <Typography 
      color="textSecondary" 
      gutterBottom
      >
        {data.last_name}
      </Typography>
      <Typography 
      color="textSecondary" 
      gutterBottom
      >
        {data.email}
      </Typography>
      <Typography 
      color="textSecondary" 
      gutterBottom
      >
        {data.phone}
      </Typography>
      <Typography 
      color="textSecondary" 
      gutterBottom
      >
        {data.gender}
      </Typography>
      <Typography 
      color="textSecondary" 
      gutterBottom
      >
        {data.address}
      </Typography>
      
    </CardContent>
    <CardActions>
        <Button 
        size="small" 
        color="primary" 
        onClick={() => onUserUpdated(data)}
        >
        Edit
        </Button>
        <Button size="small" color="secondary" onClick={deleteUser}>Delete</Button>
   </CardActions>
  </Card>  
    );
};

export default User;

